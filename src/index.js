import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Profile from "./components/Profile/Profile";

ReactDOM.render(<Profile />, document.getElementById("root"));
