import React from "react";
import "./Avatar.css";

function Avatar(props) {
  const { firstname, lastname } = props.profile;

  return (
    <div className="Avatar">
      <img
        className="Avatar-img"
        src="https://placeimg.com/150/150/people"
        alt="avatar"
      />
      <p className="Avatar-name">
        {firstname} {lastname}
      </p>
    </div>
  );
}

export default Avatar;
