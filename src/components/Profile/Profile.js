import React, { Component } from "react";
import "./Profile.css";
import ProfileHeader from "./ProfileHeader/ProfileHeader";
import ProfileDetails from "./ProfileDetails/ProfileDetails";
import Avatar from "../common/Avatar/Avatar";
import { getProfile } from "../../api/profileApi";

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      profile: null
    };
  }

  componentDidMount() {
    // Haven't implemented any sort error handling
    getProfile().then(profile => this.setState({ profile, loading: false }));
  }

  render() {
    if (this.state.loading) {
      return <p>Loading...</p>;
    }

    const { profile } = this.state;

    return (
      <div className="Profile">
        <ProfileHeader>
          <Avatar profile={profile} />
        </ProfileHeader>
        <ProfileDetails profile={profile} />
      </div>
    );
  }
}
export default Profile;
