import React from "react";
import "./ProfileDetailsItem.css";

function ProfileDetailsItem(props) {
  console.log(props);
  return (
    <div className="ProfileDetailsItem">
      <div className="ProfileDetailsItem-title">{props.title}</div>
      <div className="ProfileDetailsItem-value">{props.value}</div>
    </div>
  );
}

export default ProfileDetailsItem;
