import React from "react";
import "./ProfileDetails.css";
import ProfileDetailsItem from "./ProfileDetailsItem/ProfileDetailsItem";

function ProfileDetails(props) {
  console.log(props);

  const { email, sex } = props.profile;

  return (
    <div className="ProfileDetails">
      <ProfileDetailsItem title="title" value={email} pera="zare" />
      <ProfileDetailsItem title="sex" value={sex} />
    </div>
  );
}

export default ProfileDetails;
