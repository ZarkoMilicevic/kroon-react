import React from "react";
import "./ProfileHeader.css";

function ProfileHeader(props) {
  return <div className="ProfileHeader">{props.children}</div>;
}

export default ProfileHeader;
