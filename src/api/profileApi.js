import axios from "axios-jsonp-pro";

const BASE_URL = `http://api.gymfire.com/v01`;

function getProfile() {
  return axios.jsonp(`${BASE_URL}/testApi.ashx`).then(response => response);
}

export { getProfile };
